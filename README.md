# PyO3 and Rust - an experimental online meetup

Online version: https://g-braeunlich.gitlab.io/pyo3-rust-meetup/

## Use locally

```bash
cp -r reveal.js public
cp -r html/* public/
```

Then view `public/index.html` in your browser or run `npm start` in `public`.
